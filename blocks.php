<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.voopress.com
 * @since             1.0.0
 * @package           Blocks
 *
 * @wordpress-plugin
 * Plugin Name:       Blocks
 * Plugin URI:        http://www.voopress.com/plugins/blocks/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress dashboard.
 * Version:           1.0.0
 * Author:            Vito Calderaro
 * Author URI:        http://vito.voopress.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       blocks
 * Domain Path:       /languages
 */

add_action( 'init', 'blocks_init' );

function blocks_init() {
	// Setup Blocks
	$labels = array(
	    'name' => __( 'Block', 'block' ),
		'singular_name' => __( 'Block', 'block' ),
	    'add_new' => __( 'Add New' , 'block' ),
	    'add_new_item' => __( 'Add New Block' , 'block' ),
	    'edit_item' =>  __( 'Edit Block' , 'block' ),
	    'new_item' => __( 'New Block' , 'block' ),
	    'view_item' => __('View Block', 'block'),
	    'search_items' => __('Search Blocks', 'block'),
	    'not_found' =>  __('No Blocks found', 'block'),
	    'not_found_in_trash' => __('No Blocks found in Trash', 'block'),
	);

	register_post_type('block', array(
		'labels' => $labels,
		'public' => true,
		'show_ui' => true,
		'menu_icon' => 'dashicons-screenoptions',
		'_builtin' =>  false,
		'capability_type' => 'page',
		'hierarchical' => true,
		'rewrite' => false,
		'query_var' => "block",
		'exclude_from_search' => true,
		'supports' => array(
			'title',
			//'editor',
			//'page-attributes',
			//'post-formats',
			'revisions',
		),
		'show_in_menu'	=> true,
	));

	// Create TinyMCE Button
	//create_tinymce_button();

	//Load Before ACF
	//block_load_first();
}

// Extend the hierarchy
function blocks_add_slug_template( $template )
{
    $object = get_queried_object();

    if($object->post_type == 'block') {
	    return locate_template( "block-{$object->post_name}.php" );
    }
}

// Now we add the filter to the appropriate hook
function blocks_template_hierarchy()
{
    add_filter( 'single_template', 'blocks_add_slug_template', 10, 1 );
}
add_action( 'template_redirect', 'blocks_template_hierarchy', 20 );